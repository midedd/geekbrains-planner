# Planner

Application for learning purpose. Implements basic calendar planner functions.

## User features

* View your tasks on selected week.
* Create new task for yourself and/or another user.
* Mark task as completed.
* Task contains text and has one to many executors.

## Tech stack

### Backend
* .NET 5.0
* MS SQL
* EF Core

### UI
* SPA
* Ember.js

## Developing concepts
* Focus on BE, implement UI as MVP.
* Host UI inside backend to reduce hosting costs, avoid CORS and simplify the deployment.

## Hosting and deploy
* BE and UI are hosted as a single Azure App Service.
* Database is hosed as Azure SQL Database.
* UI and BE are deployed by powershell scripts.
* Database migrations are performed from powershell deployment scripts.
* BE configuration for test environments is Debug, BE configuration for staging and production environment is Release.

## Environments
*To be done*

## License
[The Unlicense](https://choosealicense.com/licenses/unlicense/)