﻿using Domain.Interfaces;

namespace Domain.Entities
{
    public class Author : IEntityWithId
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }
    }
}
