﻿using System.Text;
using Microsoft.EntityFrameworkCore.Storage;
using Npgsql.EntityFrameworkCore.PostgreSQL.Storage.Internal;

namespace EfDataAccess
{
    public class PostgreSqlGenerationHelper : NpgsqlSqlGenerationHelper
    {
        public PostgreSqlGenerationHelper(RelationalSqlGenerationHelperDependencies dependencies) : base(dependencies)
        {
        }

        public override string DelimitIdentifier(string identifier)
            => base.DelimitIdentifier(identifier.ToLower());

        public override void DelimitIdentifier(StringBuilder builder, string identifier)
            => base.DelimitIdentifier(builder, identifier.ToLower());
    }
}
