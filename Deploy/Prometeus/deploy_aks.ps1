az login

$RESOURCE_GROUP = "aks-prometheus"
$AKS_NAME = "aks1"

az group create --name $RESOURCE_GROUP --location germanywestcentral
az aks create --resource-group $RESOURCE_GROUP --name $AKS_NAME  --node-count 3 --node-vm-size Standard_B2s --generate-ssh-keys
az aks get-credentials --resource-group $RESOURCE_GROUP --name $AKS_NAME

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/kube-prometheus-stack --namespace monitoring --create-namespace

kubectl --namespace monitoring get pods -l "release=prometheus"

kubectl create secret docker-registry regcred --docker-server=https://index.docker.io/v1/ --docker-username=mided --docker-password=<pass> --docker-email=midedd@gmail.com

# kubectl port-forward --namespace monitoring svc/prometheus-kube-prometheus-prometheus 9090
# kubectl port-forward --namespace monitoring svc/prometheus-grafana 8080:80
